#include <iostream>
#include <vector>
#include <bitset>

typedef std::vector<int> int_v;

int_v g_fermat_result;

unsigned long long ModularExponentiation(unsigned long long base, unsigned long long exponent, unsigned long long modulo)
{
	unsigned long long result = 1;
	std::bitset<128> bit_exponent(exponent);
	base %= modulo;

	for (int i = 0; i < 128; i++)
	{
		if (bit_exponent[i] == 1)
		{
			result *= base;
			result %= modulo;
		}
		base *= base;
		base %= modulo;
	}
	return result;
}

void Fermat(unsigned int number)
{
	unsigned long long x, y, y_2, factor_1, factor_2;

	while (number % 2 == 0)
	{
		number /= 2;
		//std::cout << 2 << " ";
		g_fermat_result.push_back(2);
	}
	if (number > 1)
	{
		x = static_cast<unsigned long long>(ceil(sqrt(number)));
		do
		{
			y_2 = x * x - number;
			y = static_cast<unsigned long long>(floor(sqrt(y_2)));
			if (y_2 == y * y)
			{
				factor_1 = x - y;
				factor_2 = x + y;
				if (factor_1 == 1) 
					break;
				Fermat(factor_1);
				Fermat(factor_2);
				return;
			}
			x++;
		} while (x + y < number);
		//std::cout << number << " ";
		g_fermat_result.push_back(number);
	}
}

void Lucas(long long n, long long b)
{
	bool prime = false;
	int last_element = 0;
	Fermat(n - 1);
	if (ModularExponentiation(b, (n - 1), n) == 1)
	{
		for (auto element : g_fermat_result)
		{
			if (element == last_element)
				continue;

			if (ModularExponentiation(b, ((n - 1) / element), n) == 1)
			{
				std::cout << "Nierozstrzygniety\n";
				return;
			}
			last_element = element;
		}
		std::cout << "Liczba pierwsza\n";
	}
	else
	{
		std::cout << "Liczba zlozona\n";
	}
}

int main()
{
	g_fermat_result.clear();
	Lucas(27197, 6953);

	system("pause");
}